/*
 * Copyright 2013 James Allman
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import au.com.bytecode.opencsv.CSVReader;
import groovy.text.SimpleTemplateEngine;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.GnuParser;
import org.apache.commons.cli.Options;

import java.io.*;
import java.util.HashMap;

public class Main {
    private static final String version = "2013-07-24";
    private static final HashMap<String, Object> inputMap = new HashMap<String, Object>();

    public static final java.util.logging.Level DEBUG = java.util.logging.Level.FINE;
    public static final java.util.logging.Level TRACE = java.util.logging.Level.FINEST;

    public static void main(String[] args) {
        Options options = new Options();
        options.addOption("version", false, "print the version");

        CommandLineParser commandLineParser = new GnuParser();
        CommandLine commandLine = null;
        try {
            commandLine = commandLineParser.parse(options, args);
        } catch (Exception e) {
            e.printStackTrace();
            System.exit(1);
        }

        if (commandLine.hasOption("version")) {
            System.out.println(version);
            System.exit(1);
        }

        args = commandLine.getArgs();
        if (args.length != 2) {
            System.err.println("2 parameters required");
            System.exit(1);
        }
        csv(commandLine.getArgs());
    }

    private static void csv(String[] args) {
        try {
            CSVReader reader = new CSVReader(new FileReader(args[0]));
            String[] headerLine;
            String[] nextLine;
            int row = 0;
            // header
            if ((headerLine = reader.readNext()) != null) {
                row++;
                // detail
                while ((nextLine = reader.readNext()) != null) {
                    row++;
                    if (nextLine.length > 1 || nextLine[0].length() > 0) {
                        if (nextLine.length != headerLine.length) {
                            // abend if detail column count is not equal to header column count
                            System.err.println(String.format("Found %d columns but expecting %d in row %d!",
                                    headerLine.length, nextLine.length, row));
                            throw new Exception();
                        }
                        // convert array to map using headers
                        inputMap.clear();
                        int i = 0;
                        for (String header : headerLine) {
                            inputMap.put(header, nextLine[i]);
                            i++;
                        }
                        run(args[1]);
                    }
                }
                reader.close();
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static void run(String filename) throws IOException {
        SimpleTemplateEngine simpleTemplateEngine = new SimpleTemplateEngine();
        File file = new File("output.tmp");
        if (file.exists()) {
            file.delete();
        }
        simpleTemplateEngine
                .createTemplate(new FileReader(filename))
                .make(inputMap)
                .writeTo(new FileWriter(file));
        // rename temporary file to permanent file
        if (inputMap.containsKey("filename")) {
            filename = inputMap.get("filename").toString();
//            file.renameTo(new File(inputMap.get("filename")));
            file.renameTo(new File(filename));
        } else {
            System.err.println("Permanent filename not provided.");
            file.delete();
        }
    }
}
